package org.hnau.api.server.ktor

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.response.respondBytes
import io.ktor.response.respondText
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.extensions.asHexToBytes
import org.hnau.base.extensions.toHexString

fun KtorApiServer(
        port: Int,
        path: String = "/",
        handler: suspend BytesProvider.() -> (BytesReceiver.() -> Unit)
): ApplicationEngine {

    return embeddedServer(
            factory = Netty,
            port = port
    ) {
        routing {
            post(path) {
                val requestBytes = call.receiveText().asHexToBytes()
                val requestBytesProvider = BufferedBytesProvider(requestBytes)
                val responseBuilder = handler(requestBytesProvider)
                val responseBytes = BufferedBytesReceiver().apply(responseBuilder).buffer
                call.respondText(
                        text = responseBytes.toHexString(),
                        status = HttpStatusCode.OK,
                        contentType = ContentType.Any
                )
            }
        }
    }
}